CREATE TABLE IF NOT EXISTS `bit24_promotions_gift_campaign` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE_FROM` datetime DEFAULT NULL,
  `ACTIVE_TO` datetime DEFAULT NULL,
  `MULTIPLE_APPLY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `B24_ACTIVE_DATES` (`ACTIVE`,`ACTIVE_FROM`,`ACTIVE_TO`)
);

CREATE TABLE IF NOT EXISTS `bit24_promotions_gift_campaign_target_product` (
  `CAMPAIGN_ID` int(18) NOT NULL,
  `PRODUCT_ID` int(18) NOT NULL,
  `AMOUNT` int(18) DEFAULT 1,
  PRIMARY KEY (`CAMPAIGN_ID`,`PRODUCT_ID`)
);

CREATE TABLE IF NOT EXISTS `bit24_promotions_gift_campaign_gift_product` (
  `CAMPAIGN_ID` int(18) NOT NULL,
  `PRODUCT_ID` int(18) NOT NULL,
  `AMOUNT` int(18) DEFAULT 1,
  PRIMARY KEY (`CAMPAIGN_ID`,`PRODUCT_ID`)
);

CREATE TABLE IF NOT EXISTS `bit24_promotions_gift_campaign_gift_bucket` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CAMPAIGN_ID` int(18) NOT NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE IF NOT EXISTS `bit24_promotions_gift_campaign_gift_bucket_product` (
  `BUCKET_ID` int(18) NOT NULL,
  `PRODUCT_ID` int(18) NOT NULL,
  `AMOUNT` int(18) DEFAULT 1,
  PRIMARY KEY (`BUCKET_ID`,`PRODUCT_ID`)
);
