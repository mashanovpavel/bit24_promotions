function ListOfLinkedProducts(name){
	var self = this;

	var index = 0;
	var newRows = 0;

	var rootEl = null;
	var addRowButtonEl = null;
	var deleteRowButtonsEl = null;
	var tableBodyEL = null;
	var rowTemplateEl = null;

	var escapeFunctionName = function(fname) {
		fname = fname.replace("[","_");
		fname = fname.replace("]","_");

		return fname;
	};

	var escapedName = escapeFunctionName(name);
	var bindEvents = function() {};

	var grepElements = function() {
		var rootId = escapedName + "-listoflinkedproductstable";
		rootEl = $("#" + rootId).first();
		addRowButtonEl = rootEl.siblings(".js-add-row-"+escapedName).first();

		deleteRowButtonsEl = $(".js-delete-listoflinkedproducts-row-button", rootEl).not(".js-listoflinkedproducts-row-template");
		tableBodyEL = $("tbody", rootEl).first();
		rowTemplateEl = $(".js-listoflinkedproducts-row-template", rootEl);
	};

	var addRow = function()
	{
		newRows++;

		var fieldName = name +  "[NEW_" + newRows + "]";

		//Добавить строку
		var html = rowTemplateEl.html();
		html = html.replace(/#INDEX#/g, index);
		var newRow = $('<tr class="js-list-row" data-basenumber="'+newRows+'">' + html + '</tr>');

		$(".js-id-cell span", newRow).html("Новый " + newRows);

		//Создать новый список в ячейке
		var cell = newRow.find(".js-panel-cell").first();

		rowTemplateEl.before(newRow);

		LinkedProducts.createInContainer(fieldName, cell);

		grepElements();
		bindEvents();
	};

	var onAddRowClick = function(e) {
		e.preventDefault();

		addRow();
	};

	var onDeleteRowClick = function(e) {
		e.preventDefault();

		$(this).closest("tr").remove();
	};

	//Публичный интерфейс
	//отсутствует

	bindEvents = function() {
		addRowButtonEl.unbind("click", onAddRowClick).bind("click", onAddRowClick);
		deleteRowButtonsEl.unbind("click", onDeleteRowClick).bind("click", onDeleteRowClick);
	};

	grepElements();
	bindEvents();
}
