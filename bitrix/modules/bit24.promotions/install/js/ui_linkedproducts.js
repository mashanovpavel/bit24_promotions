function LinkedProducts(name, index){
	var self = this;

	var rootEl = null;
	var addRowButtonEl = null;
	var addRowsButtonEl = null;
	var deleteRowButtonsEl = null;
	var changeRowButtonsEl = null;
	var tableBodyEL = null;
	var rowTemplateEl = null;

	var bindEvents = function() {};

	var escapeFunctionName = function(fname) {
		fname = fname.replace("[","_");
		fname = fname.replace("]","_");

		return fname;
	};

	var grepElements = function() {
		var escapedName = escapeFunctionName(name);

		var rootId = escapedName + "-linkedproductstable";
		rootEl = $("#" + rootId).first();

		addRowButtonEl = rootEl.siblings(".js-add-row-"+escapedName).first();
		addRowsButtonEl = rootEl.siblings(".js-add-rows-"+escapedName).first();
		deleteRowButtonsEl = $(".js-delete-row-button", rootEl);
		changeRowButtonsEl = $(".js-change-product-button", rootEl);
		tableBodyEL = $("tbody", rootEl).first();
		rowTemplateEl = $(".js-product-row-template", rootEl);
	};

	var registerGlobalCallbackForBitrixElementSelectPopup = function()
	{
		var functionName = "InS" + md5(name + "multyadd");

		window[escapeFunctionName(functionName)] = function(id, name) {
			self.AddValue(id, name);
		};

		functionName = "linkedProductsLookup_" + name;
		window[escapeFunctionName(functionName)] = self;
	};

	var addRow = function(id, name, amount)
	{
		index++;

		var html = rowTemplateEl.html();
		html = html.replace(/#INDEX#/g, index);
		var newRow = $('<tr class="js-product-row" data-basenumber="'+index+'">' + html + '</tr>');

		if (id) {
			$('.js-change-product-cell input', newRow).first().val(id);
		}
		if (name) {
			$('.js-product-cell span', newRow).first().html(name);
		}
		if (amount) {
			$('.js-amount-cell input', newRow).first().val(amount);
		}

		tableBodyEL.find("tr").last().before(newRow);

		grepElements();
		bindEvents();
		registerGlobalCallbackForBitrixElementSelectPopup();
	};

	var addRows = function(rowIndex) {
		var urlToCall = '/bitrix/admin/iblock_element_search.php?lang=ru&IBLOCK_ID=7&n=' + name + 'multyadd' + '&k=' + rowIndex + '&m=y';
		jsUtils.OpenWindow(urlToCall, 600, 500);
	};

	var changeRow = function(rowIndex) {
		var urlToCall = '/bitrix/admin/iblock_element_search.php?lang=ru&IBLOCK_ID=7&n=' + name + '[PRODUCT]&k=' + rowIndex + '';
		jsUtils.OpenWindow(urlToCall, 600, 500);
	};

	var onAddRowsClick = function(e) {
		e.preventDefault();

		addRows(index);
	};

	var onDeleteRowClick = function(e) {
		e.preventDefault();

		$(this).closest("tr").remove();
	};

	var onChangeRowClick = function(e) {
		e.preventDefault();

		var rowIndex = $(this).closest("tr").data("basenumber");
		changeRow(rowIndex);
	};

	//Публичный интерфейс
	self.AddValue = function(productId, productName) {
		addRow(productId, productName, 1);
	};

	bindEvents = function() {
		addRowsButtonEl.unbind("click", onAddRowsClick).bind("click", onAddRowsClick);
		deleteRowButtonsEl.unbind("click", onDeleteRowClick).bind("click", onDeleteRowClick);
		changeRowButtonsEl.unbind("click", onChangeRowClick).bind("click", onChangeRowClick);
	};

	grepElements();
	bindEvents();
	registerGlobalCallbackForBitrixElementSelectPopup();
}


LinkedProducts.createInContainer = function(name, container) {

	var escapeFunctionName = function(fname) {
		fname = fname.replace("[","_");
		fname = fname.replace("]","_");

		return fname;
	};

	var escName = escapeFunctionName(name);

	var html = "";

	html += '<div class="product-table-wrapper">'
		+ '<table id="' + escName + '-linkedproductstable" class="linkedproductstable-table">'
		+ '<tbody>';

	html += '<tr class="js-product-row-template" data-basenumber="#INDEX#" style="display: none">'
		+ '<td class="js-change-product-cell">'
		+ '<input type="text" name="'+name+'[PRODUCT][#INDEX#]" value="" id="'+name+'[PRODUCT][#INDEX#]" size="6"/>'
		+	'</td><td>'
		+ '<input type="button" value="..." class="js-change-product-button">'
		+ '</td>'
		+ '<td class="js-product-cell" data-productId="">'
		+ '<span id="sp_' + md5(name+"[PRODUCT]")+ '_#INDEX#"></span>'
		+ '</td>'
		+ '<td class="js-amount-cell"><input type="text" name="'+name+'[AMOUNT][#INDEX#]" value="1" size="4"/></td>'
		+ '<td class="js-button-cell"><input type="button" class="js-delete-row-button" value="x"/></td>'
		+ '</tr>';

	html += '</tbody>'
		+ '</table>'
		+ '<input type="button" value="Добавить товары" class="js-add-rows-' + escName + '">';

	html += '</div>';

	container.html(html);

	new LinkedProducts(name, 0);
};