<?php

// admin initialization
define("ADMIN_MODULE_NAME", "bit24.promotions");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

global $APPLICATION, $USER, $USER_FIELD_MANAGER;

IncludeModuleLangFile(__FILE__);

if (!$USER->IsAdmin())
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

if (!CModule::IncludeModule(ADMIN_MODULE_NAME))
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}
if (!CModule::IncludeModule("iblock"))
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

use \Bit24\Promotions\GiftCampaignTable;

$APPLICATION->SetTitle("Список подарочных кампаний");

$entity_table_name = GiftCampaignTable::getTableName();
$sTableID = 'tbl_'.$entity_table_name;
$oSort = new CAdminSorting($sTableID, "ID", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arHeaders = array(
	array(
       'id' => 'ID',
       'content' => 'ID',
       'sort' => 'ID',
       'default' => true
   ),
	array(
		'id' => 'ACTIVE',
		'content' => 'Активность',
		'sort' => 'ACTIVE',
		'default' => false,
	),
	array(
		'id' => 'NAME',
		'content' => 'Название',
		'sort' => 'NAME',
		'default' => false,
	),
	array(
		'id' => 'ACTIVE_FROM',
		'content' => 'Дата начала активности',
		'sort' => 'ACTIVE_FROM',
		'default' => false,
	),
	array(
		'id' => 'ACTIVE_TO',
		'content' => 'Дата конца активности',
		'sort' => 'ACTIVE_TO',
		'default' => false,
	),
	array(
		'id' => 'MULTIPLE_APPLY',
		'content' => 'Применять несколько раз',
		'sort' => 'ACTIVE',
		'default' => false,
	),
	array(
		'id' => 'TARGETS',
		'content' => 'Цели',
		'sort' => 'TARGETS',
		'default' => false,
	),
	array(
		'id' => 'GIFTS',
		'content' => 'Подарки',
		'sort' => false,
		'default' => false,
	),
	array(
		'id' => 'BUCKETS',
		'content' => 'Наборы',
		'sort' => false,
		'default' => false,
	),
);

// show all columns by default
foreach ($arHeaders as &$arHeader)
{
	$arHeader['default'] = true;
}
unset($arHeader);

$lAdmin->AddHeaders($arHeaders);

if (!in_array($by, $lAdmin->GetVisibleHeaderColumns(), true))
{
	$by = 'ID';
}

// add filter
$filter = null;

$filterFields = array('find_id');
$filterValues = array();
$filterTitles = array('ID');

$USER_FIELD_MANAGER->AdminListAddFilterFields($ufEntityId, $filterFields);

$filter = $lAdmin->InitFilter($filterFields);

if (!empty($find_id))
{
	$filterValues['ID'] = $find_id;
}

$USER_FIELD_MANAGER->AdminListAddFilter($ufEntityId, $filterValues);
$USER_FIELD_MANAGER->AddFindFields($ufEntityId, $filterTitles);

$filter = new CAdminFilter(
	$sTableID."_filter_id",
	$filterTitles
);


// group actions
if($lAdmin->EditAction())
{
	foreach($FIELDS as $ID=>$arFields)
	{
		$ID = IntVal($ID);

		if(!$lAdmin->IsUpdated($ID))
			continue;

		GiftCampaignTable::update($ID, $arFields);
	}
}

if($arID = $lAdmin->GroupAction())
{
	if($_REQUEST['action_target']=='selected')
	{
		$arID = array();

		$rsData = GiftCampaignTable::getList(array(
			"select" => array('ID'),
			"filter" => $filterValues
		));

		while($arRes = $rsData->Fetch())
			$arID[] = $arRes['ID'];
	}

	foreach ($arID as $ID)
	{
		$ID = intval($ID);

		if (!$ID)
		{
			continue;
		}

		switch($_REQUEST['action'])
		{
			case "delete":
				GiftCampaignTable::delete($ID);
				break;
		}
	}
}

$arr = array('delete' => true);
$lAdmin->AddGroupActionTable($arr);

// select data
/** @var string $order */
//Цели и подарки выбираиюся внутри цикла
$arSelect = array_diff(
	$lAdmin->GetVisibleHeaderColumns(),
	array("TARGETS"),
	array("GIFTS"),
	array("BUCKETS")
);

$rsData = GiftCampaignTable::getList(array(
	"select" => $arSelect,
	"filter" => $filterValues,
	"order" => array($by => strtoupper($order))
));

$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();

// build list
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("PAGES")));
while($arRes = $rsData->NavNext(true, "f_"))
{
	$arRes["ACTIVE"] = $arRes["ACTIVE"] == "Y" ? "Да" : "Нет";
	$arRes["MULTIPLE_APPLY"] = $arRes["MULTIPLE_APPLY"] == "Y" ? "Да" : "Нет";

	$row = $lAdmin->AddRow($f_ID, $arRes);

	//Цели
	$arTargets = \Bit24\Promotions\GiftCampaignTargetProductTable::getForCampaign((int)$f_ID, true);
	$arTargetsView = array();
	foreach ($arTargets as $arTarget)
	{
		$amount = (int) $arTarget["AMOUNT"];
		$productId = $arTarget["PRODUCT_ID"];
		$productIblockId = $arTarget["IBLOCK_ID"];
		$name = htmlspecialcharsbx($arTarget["NAME"]);

		$elementLink = CIBlock::GetAdminElementEditLink($productIblockId, $productId);

		$arTargetsView[] = $amount.' шт. [<a href="'.$elementLink.'">'.$productId."</a>] $name";
	}
	$row->AddViewField("TARGETS", implode(" <hr> ", $arTargetsView));

	//Подарки
	$arGifts = \Bit24\Promotions\GiftCampaignGiftProductTable::getForCampaign((int)$f_ID, true);
	$arGiftsView = array();
	foreach ($arGifts as $arGift)
	{
		$amount = (int) $arGift["AMOUNT"];
		$productId = $arGift["PRODUCT_ID"];
		$productIblockId = $arGift["IBLOCK_ID"];
		$name = htmlspecialcharsbx($arGift["NAME"]);

		$elementLink = CIBlock::GetAdminElementEditLink($productIblockId, $productId);

		$arGiftsView[] = $amount.' шт. [<a href="'.$elementLink.'">'.$productId."</a>] $name";
	}
	$row->AddViewField("GIFTS", implode(" <hr> ", $arGiftsView));

	//Наборы
	//TODO: Переписать на один выбор всех нужных товаров, без подзапросов в цикле.
	$bucketsView = array();
	$bucketsForCampaign = \Bit24\Promotions\GiftCampaignGiftBucketProductTable::getForCampaign((int)$f_ID, true);
	foreach ($bucketsForCampaign as $bucketId=>$products)
	{
		$bucketView = array();
		foreach ($products as $arProductRow)
		{
			$productId =  $arProductRow["PRODUCT_ID"];
			$amount = $arProductRow["AMOUNT"];
			$productIblockId = $arProductRow["IBLOCK_ID"];
			$name = htmlspecialcharsbx($arProductRow["NAME"]);

			$elementLink = CIBlock::GetAdminElementEditLink($productIblockId, $productId);

			$bucketView[] = $amount.' шт. [<a href="'.$elementLink.'">'.$productId."</a>] $name";
		}
		$bucketsView[] = $bucketView;
	}

	$aBuckets = array();
	foreach ($bucketsView as $bucketsViewRow)
	{
		$aBuckets[] = implode(" <br> ", $bucketsViewRow);
	}
	$row->AddViewField("BUCKETS", implode(" <hr><br> ", $aBuckets));

	$can_edit = true;

	$arActions = Array();

	$arActions[] = array(
		"ICON" => "edit",
		"TEXT" => GetMessage($can_edit ? "MAIN_ADMIN_MENU_EDIT" : "MAIN_ADMIN_MENU_VIEW"),
		"ACTION" => $lAdmin->ActionRedirect("bit24.promotions_gift_edit.php?ID=".$f_ID),
		"DEFAULT" => true
	);

	$arActions[] = array(
		"ICON"=>"delete",
		"TEXT" =>"Удалить",
		"ACTION" => "if(confirm('".GetMessageJS('HLBLOCK_ADMIN_DELETE_ROW_CONFIRM')."')) ".
			$lAdmin->ActionRedirect("bit24.promotions_gift_edit.php?action=delete&ID=".$f_ID.'&'.bitrix_sessid_get())
	);

	$row->AddActions($arActions);
}

// view
//TODO:
$lAdmin->AddAdminContextMenu(array(array(
	                                   "TEXT"	=> 'Добавить кампанию',
	                                   "TITLE"	=> 'Добавить кампанию',
	                                   "LINK"	=> "bit24.promotions_gift_edit.php?lang=".LANGUAGE_ID,
	                                   "ICON"	=> "btn_new"
                                   )));

$lAdmin->CheckListMode();

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

?>
	<form name="find_form" method="GET" action="<?echo $APPLICATION->GetCurPage()?>">
		<?
		$filter->Begin();
		?>
		<tr>
			<td>ID</td>
			<td><input type="text" name="find_id" size="47" value="<?echo htmlspecialcharsbx($find_id)?>"><?=ShowFilterLogicHelp()?></td>
		</tr>
		<?
		$USER_FIELD_MANAGER->AdminListShowFilter($ufEntityId);
		$filter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPage().'?ENTITY_ID='.$hlblock['ID'], "form"=>"find_form"));
		$filter->End();
		?>
	</form>
<?

$lAdmin->DisplayList();

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");