<?php

// admin initialization
define("ADMIN_MODULE_NAME", "bit24.promotions");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

IncludeModuleLangFile(__FILE__);

if (!$USER->IsAdmin())
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

if (!CModule::IncludeModule(ADMIN_MODULE_NAME))
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

if (!CModule::IncludeModule("iblock"))
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

use Bit24\Promotions\GiftCampaignTable;

if ($_REQUEST["ID"] && !$arCurrent)
{
	$arCurrent = GiftCampaignTable::getById((int)$_REQUEST["ID"])->fetch();

	if(!$arCurrent) {
		// 404
		if ($_REQUEST["mode"] == "list")
		{
			require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");
		}
		else
		{
			require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
		}

		echo "Запись не найдена.";

		if ($_REQUEST["mode"] == "list")
		{
			require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php");
		}
		else
		{
			require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
		}

	die();
	}
}

$is_create_form = true;
$is_update_form = false;

$isEditMode = true;

$errors = array();

// get row
$row = null;

if (isset($_REQUEST['ID']) && $_REQUEST['ID'] > 0)
{
	$row = GiftCampaignTable::getById($_REQUEST['ID'])->fetch();

	if (!empty($row))
	{
		$is_update_form = true;
		$is_create_form = false;
	}
	else
	{
		$row = null;
	}
}

if ($is_create_form)
{
	$APPLICATION->SetTitle("Добавление");
}
else
{
	$APPLICATION->SetTitle("Редактирование");
}

// form
$aTabs = array(
	array("DIV" => "edit1", "TAB" => "Подарочная кампания", "ICON"=>"ad_contract_edit", "TITLE"=> "Подарочная кампания"),
	array("DIV" => "targets", "TAB" => "Цели", "ICON"=>"ad_contract_edit", "TITLE"=> "Цели"),
	array("DIV" => "gifts", "TAB" => "Подарки", "ICON"=>"ad_contract_edit", "TITLE"=> "Подарки"),
	array("DIV" => "buckets", "TAB" => "Наборы", "ICON"=>"ad_contract_edit", "TITLE"=> "Наборы"),
);

$tabControl = new CAdminForm("edit_".GiftCampaignTable::getTableName(), $aTabs);

// delete action
if ($is_update_form && isset($_REQUEST['action']) && $_REQUEST['action'] === 'delete' && check_bitrix_sessid())
{
	GiftCampaignTable::delete($row['ID']);

	LocalRedirect("bit24.promotions_gift_list.php?lang=".LANGUAGE_ID);
}

$targetTable = new Bit24\Promotions\UI\LinkedProducts("TARGETS",array());
$giftTable = new Bit24\Promotions\UI\LinkedProducts("GIFTS", array());
$bucketList = new Bit24\Promotions\UI\ListOfLinkedProducts("BUCKETS", array());

// save action
if ((strlen($save)>0 || strlen($apply)>0) && $REQUEST_METHOD=="POST" && check_bitrix_sessid())
{
	$data = array();

	$data["ACTIVE"] = $ACTIVE == "Y" ? "Y" : "N";
	$data["MULTIPLE_APPLY"] = $MULTIPLE_APPLY == "Y" ? "Y" : "N";
	$data["NAME"] = $NAME;
	$data["ACTIVE_FROM"] = mb_strlen($ACTIVE_FROM) ? \Bitrix\Main\Type\DateTime::createFromUserTime($ACTIVE_FROM) : null;
	$data["ACTIVE_TO"] =  mb_strlen($ACTIVE_TO) ? \Bitrix\Main\Type\DateTime::createFromUserTime($ACTIVE_TO) : null;

	$arTargets = $targetTable->parseForm($_REQUEST);

	$arGifts = $giftTable->parseForm($_REQUEST);

	$arBuckets =$bucketList->parseForm($_REQUEST);

	/** @param Bitrix\Main\Entity\AddResult $result */
	if ($is_update_form)
	{
		$ID = intval($_REQUEST['ID']);
		$result = GiftCampaignTable::saveWithTargetsAndGiftsAndBuckets($ID, $data, $arTargets, $arGifts, $arBuckets);
	}
	else
	{
		$result = GiftCampaignTable::saveWithTargetsAndGiftsAndBuckets(0, $data, $arTargets, $arGifts, $arBuckets);
		$ID = $result->getId();
	}

	if($result->isSuccess())
	{
		if (strlen($save)>0)
		{
			LocalRedirect("bit24.promotions_gift_list.php?lang=".LANGUAGE_ID);
		}
		else
		{
			LocalRedirect("bit24.promotions_gift_edit.php?ID=".intval($ID)."&lang=".LANGUAGE_ID."&".$tabControl->ActiveTabParam());
		}
	}
	else
	{
		$errors = $result->getErrorMessages();
	}
}

// menu
$aMenu = array(
	array(
		"TEXT"	=> "К списку",
		"TITLE"	=> "К списку",
		"LINK"	=> "bit24.promotions_gift_list.php?lang=".LANGUAGE_ID,
		"ICON"	=> "btn_list",
	)
);

$context = new CAdminContextMenu($aMenu);

if($is_update_form && !$errors)
{
	$row = GiftCampaignTable::getById($_REQUEST['ID'])->fetch();

	$ACTIVE = $row['ACTIVE'];
	$NAME = $row['NAME'];
	$MULTIPLE_APPLY = $row['MULTIPLE_APPLY'];

	/** @var \Bitrix\Main\Type\DateTime */
	$fromDt = $row['ACTIVE_FROM'];
	$ACTIVE_FROM = is_object($fromDt) ? $fromDt->toString() : "";

	/** @var \Bitrix\Main\Type\DateTime */
	$toDt = $row['ACTIVE_TO'];
	$ACTIVE_TO = is_object($toDt) ? $toDt->toString() : "";

	$targets = \Bit24\Promotions\GiftCampaignTargetProductTable::getForCampaign((int)$row["ID"]);
	$targetTable->setValues($targets);

	$gifts = \Bit24\Promotions\GiftCampaignGiftProductTable::getForCampaign((int)$row["ID"]);
	$giftTable->setValues($gifts);

	$buckets = \Bit24\Promotions\GiftCampaignGiftBucketProductTable::getForCampaign((int)$row["ID"]);
	$bucketList->setValues($buckets);
}
 else
{
	$targetTable->setValues($targetTable->parseForm($_REQUEST));
	$giftTable->setValues($giftTable->parseForm($_REQUEST));
	$bucketList->setValues($bucketList->parseForm($_REQUEST));
}

//view

if ($_REQUEST["mode"] == "list")
{
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");
}
else
{
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
}

$context->Show();

if (!empty($errors))
{
	CAdminMessage::ShowMessage(join("\n", $errors));
}

$tabControl->BeginPrologContent();

echo CAdminCalendar::ShowScript();

$tabControl->EndPrologContent();
$tabControl->BeginEpilogContent();
?>

	<?=bitrix_sessid_post()?>
	<input type="hidden" name="ID" value="<?=htmlspecialcharsbx(!empty($row)?$row['ID']:'')?>">
	<input type="hidden" name="lang" value="<?=LANGUAGE_ID?>">

	<?$tabControl->EndEpilogContent();?>

	<? $tabControl->Begin(array(
		"FORM_ACTION" => $APPLICATION->GetCurPage()."?ID=".IntVal($ID)."&lang=".LANG
	));?>

	<? $tabControl->BeginNextFormTab(); ?>

	<? $tabControl->AddViewField("ID", "ID", !empty($row)?$row['ID']:''); ?>

	<?$tabControl->AddCheckBoxField("ACTIVE", "Активность:", false, "Y",$ACTIVE=="Y");?>
	<?$tabControl->AddEditField("NAME", "Название:", true, array(), $NAME);?>
	<?
	$tabControl->BeginCustomField("ACTIVE_FROM", "Дата начала активности", false);
	?>
	<tr id="tr_ACTIVE_FROM">
		<td><?echo $tabControl->GetCustomLabelHTML()?>:</td>
		<td><?echo CAdminCalendar::CalendarDate("ACTIVE_FROM", $ACTIVE_FROM, 19, true)?></td>
	</tr>
	<?
	$tabControl->EndCustomField("ACTIVE_FROM");
	$tabControl->BeginCustomField("ACTIVE_TO", "Дата конца активности", false);
	?>
	<tr id="tr_ACTIVE_TO">
		<td><?echo $tabControl->GetCustomLabelHTML()?>:</td>
		<td><?echo CAdminCalendar::CalendarDate("ACTIVE_TO", $ACTIVE_TO, 19, true)?></td>
	</tr>

	<?
	$tabControl->EndCustomField("ACTIVE_TO");
	?>
	<?$tabControl->AddCheckBoxField("MULTIPLE_APPLY", "Применять несколько раз:", false, "Y", $MULTIPLE_APPLY=="Y");?>
	<? $tabControl->BeginNextFormTab(); ?>
	<?
	$tabControl->BeginCustomField("TARGETS", "Цели", false);
	?>
	<tr id="tr_TARGETS">
		<td><?echo $tabControl->GetCustomLabelHTML()?>:</td>
		<td>
			<?$targetTable->show()?>
		</td>
	</tr>
	<?
	$tabControl->EndCustomField("TARGETS");
	?>
	<? $tabControl->BeginNextFormTab(); ?>
	<?
	$tabControl->BeginCustomField("GIFTS", "Подарки", false);
	?>
	<tr id="tr_GIFTS">
		<td><?echo $tabControl->GetCustomLabelHTML()?>:</td>
		<td>
			<?$giftTable->show()?>
		</td>
	</tr>
	<?
	$tabControl->EndCustomField("GIFTS");
	?>
	<? $tabControl->BeginNextFormTab(); ?>
	<?
	$tabControl->BeginCustomField("BUCKETS", "Наборы", false);
	?>
	<tr id="tr_BUCKETS">
		<td><?echo $tabControl->GetCustomLabelHTML()?>:</td>
		<td>
			<?$bucketList->show()?>
		</td>
	</tr>
	<?
	$tabControl->EndCustomField("BUCKETS");
	?>
	<?

	$tabControl->Buttons(array("disabled" => $disable, "back_url"=>"bit24.promotions_gift_list.php?lang=".LANGUAGE_ID));

	$tabControl->Show();
	?>

<?echo BeginNote();?>
<ul>
	<li>Если не указаны цели, то подарки будут добавлены один раз, если в корзине есть хотя бы один товар за деньги.</li>
	<li>Если установлено "Применять несколько раз", подарки будут добавлены N раз, если каждая цель найдена как минимум в количестве N * кол-во цели.<br> Например: Цель - 2 товара А, Подарки - 1 и 2 товара B и C соответственно. Если товара A в корзине будет 4, то подарками будут 2 B и 4 C.</li>
	<li>Цели и подарки могут совпадать.</li>
	<li>Пустые наборы удаляются.</li>
</ul>
<?echo EndNote();?>

<?

if ($_REQUEST["mode"] == "list")
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php");
else
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
