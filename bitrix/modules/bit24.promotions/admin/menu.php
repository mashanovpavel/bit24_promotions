<?php
/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 */

global $DBType, $adminMenu, $adminPage;

$MODULE_ID = basename(dirname(__DIR__));

$aMenu = array(
	"parent_menu" => "global_menu_store",
	"section" => $MODULE_ID,
	"sort" => 10,
	"text" => "Бит24. Промо акции",
	"title" => "Бит24. Промо акции",
	"icon" => "bit24-promotions-icon",
	"page_icon" => "",
	"items_id" => $MODULE_ID . "_items",
	"more_url" => array(),
	"items" => array(
		array(
			"text" => "Список подарочных кампаний",
			"url" => $MODULE_ID."_gift_list.php",
			"module_id" => $MODULE_ID,
			"title" => "Список подарочных кампаний",
		),
	)
);

return $aMenu;
