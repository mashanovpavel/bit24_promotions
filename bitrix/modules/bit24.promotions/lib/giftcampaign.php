<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Pavel Mashanov
 */

namespace Bit24\Promotions;

use Bitrix\Main\Application;
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\Event;
use Bitrix\Main\Localization\Loc;
use Bit24\Promotions\Entity as Own;

Loc::loadMessages(__FILE__);

/**
 * Подарочная кампания
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ACTIVE bool optional default 'Y'
 * <li> NAME string(255) optional
 * <li> ACTIVE_FROM datetime optional
 * <li> ACTIVE_TO datetime optional
 * </ul>
 * @package Bit24\Promotions
 **/
class GiftCampaignTable extends Own {
	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return 'bit24_promotions_gift_campaign';
	}

	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('GIFT_CAMPAIGN_ENTITY_ID_FIELD'),
			),
			'ACTIVE' => array(
				'data_type' => 'boolean',
				'values' => array(
					'N',
					'Y'
				),
				'title' => Loc::getMessage('GIFT_CAMPAIGN_ENTITY_ACTIVE_FIELD'),
			),
			'NAME' => array(
				'data_type' => 'string',
				'validation' => array(
					__CLASS__,
					'validateName'
				),
				'title' => Loc::getMessage('GIFT_CAMPAIGN_ENTITY_NAME_FIELD'),
			),
			'ACTIVE_FROM' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('GIFT_CAMPAIGN_ENTITY_ACTIVE_FROM_FIELD'),
			),
			'ACTIVE_TO' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('GIFT_CAMPAIGN_ENTITY_ACTIVE_TO_FIELD'),
			),
			'MULTIPLE_APPLY' => array(
				'data_type' => 'boolean',
				'values' => array(
					'N',
					'Y'
				),
				'title' => Loc::getMessage('GIFT_CAMPAIGN_ENTITY_MULTIPLE_APPLY_FIELD'),
			),

		);
	}

	/**
	 * Валидатор названия
	 * @return array
	 */
	public static function validateName()
	{
		return array(
			new Entity\Validator\Length(1, 255),
		);
	}

	/**
	 * Сохраняет кампанию с целями, подарками и наборами.
	 * Если $id == 0, то создает новую
	 *
	 * @param int   $id       ID кампании
	 * @param array $campaign данные кампании
	 * @param array $targets  данные целевых продуктов array(array( "PRODUCT_ID" => 23, "AMOUNT" => 2),...)
	 * @param array $gifts    данные подарков array(array("PRODUCT_ID" => 34, "AMOUNT" => 1),...)
	 * @param array $buckets  данные наборов подарков на выбор array("ID" => array(array("PRODUCT_ID" => 34, "AMOUNT"
	 *                        => 1),...)) важно сохранить ID, если набор не менялся
	 *
	 * @return Entity\AddResult|Entity\UpdateResult
	 */
	public static function saveWithTargetsAndGiftsAndBuckets($id, $campaign, $targets, $gifts, $buckets)
	{
		$db = Application::getConnection();

		$db->startTransaction();

		if ($id)
		{
			$result = self::update($id, $campaign);
		}
		else
		{
			$result = self::add($campaign);
			$id = $result->getId();
		}

		if ($result->isSuccess() && $id)
		{
			//Цели
			self::__saveTargets($result, $id, $targets);

			if ($result->isSuccess())
			{
				//Подарки
				self::__saveGifts($result, $id, $gifts);

				//Наборы
				if ($result->isSuccess())
				{
					self::__saveBuckets($result, $id, $buckets);

					if ($result->isSuccess())
					{
						$db->commitTransaction();
					}
				}
			}
		}
		else
		{
			$db->rollbackTransaction();
		}

		return $result;
	}

	/**
	 * Сохранение подарков в процессе сохранения кампании
	 *
	 * @param Entity\Result $result результат всей операции
	 * @param int           $id     ID кампании
	 * @param array         $gifts  подарки
	 */
	protected static function __saveGifts($result, $id, $gifts)
	{
		foreach (GiftCampaignGiftProductTable::getForCampaign($id) as $arGift)
		{
			$deleteKeys = array(
				"CAMPAIGN_ID" => $id,
				"PRODUCT_ID" => $arGift["PRODUCT_ID"]
			);
			GiftCampaignGiftProductTable::delete($deleteKeys);
		}
		foreach ($gifts as $arGift)
		{
			$arGift["CAMPAIGN_ID"] = $id;
			$r = GiftCampaignGiftProductTable::add($arGift);
			if (!$r->isSuccess())
			{
				foreach ($r->getErrorMessages() as $message)
				{
					$result->addError(new Entity\EntityError($message));
				}
			}
		}
	}

	/**
	 * Сохранение целей в процессе сохранения кампании
	 *
	 * @param Entity\Result $result  результат всей операции
	 * @param int           $id      ID кампании
	 * @param array         $targets цели
	 */
	protected static function __saveTargets($result, $id, $targets)
	{
		foreach (GiftCampaignTargetProductTable::getForCampaign($id) as $arTarget)
		{
			$deleteKeys = array(
				"CAMPAIGN_ID" => $id,
				"PRODUCT_ID" => $arTarget["PRODUCT_ID"]
			);
			GiftCampaignTargetProductTable::delete($deleteKeys);
		}
		foreach ($targets as $arTarget)
		{
			$arTarget["CAMPAIGN_ID"] = $id;
			$r = GiftCampaignTargetProductTable::add($arTarget);
			if (!$r->isSuccess())
			{
				foreach ($r->getErrorMessages() as $message)
				{
					$result->addError(new Entity\EntityError($message));
				}
			}
		}
	}

	/**
	 * Сохранение наборов в процессе сохранения кампании
	 *
	 * @param Entity\Result $result  результат всей операции
	 * @param int           $id      кампании
	 * @param array         $buckets наборы
	 */
	protected static function __saveBuckets($result, $id, $buckets)
	{
		//Текущие наборы
		$bucketId2Products = GiftCampaignGiftBucketProductTable::getForCampaign($id);

		//Определим, какие удалить, добавить, обновить,
		//Добавить
		$bucketsToAdd = array();
		foreach ($buckets as $newBucketId => $bucket)
		{
			if (!array_key_exists($newBucketId, $bucketId2Products))
			{
				$bucketsToAdd[$newBucketId] = $bucket;
			}
		}

		//Удалить
		$bucketsToDelete = array();
		foreach ($bucketId2Products as $existingBucketId => $bucket)
		{
			if (!array_key_exists($existingBucketId, $buckets))
			{
				$bucketsToDelete[$existingBucketId] = $bucket;
			}
		}

		//Все прочее - обновить
		$bucketsToUpdate = array();
		foreach ($buckets as $newBucketId => $bucket)
		{
			if (!array_key_exists($newBucketId, $bucketsToAdd) && !array_key_exists($newBucketId, $bucketsToDelete))
			{
				$bucketsToUpdate[$newBucketId] = $bucket;
			}
		}

		$allOk = true;
		//Выполним действия
		if ($allOk)
		{
			foreach ($bucketsToDelete as $existingBucketId => $bucket)
			{
				$r = GiftCampaignGiftBucketTable::delete($existingBucketId);
				$allOk = self::bubbleError($r, $result);
			}
		}
		if ($allOk)
		{
			foreach ($bucketsToUpdate as $existingBucketId => $bucketProducts)
			{
				$existingBucketProducts = $bucketId2Products[$existingBucketId];
				$r = GiftCampaignGiftBucketTable::saveWithProducts($existingBucketId, array("CAMPAIGN_ID" => $id), $bucketProducts, $existingBucketProducts);
				$allOk = self::bubbleError($r, $result);
			}
		}
		if ($allOk)
		{
			foreach ($bucketsToAdd as $existingBucketId => $bucketProducts)
			{
				$r = GiftCampaignGiftBucketTable::saveWithProducts(0, array("CAMPAIGN_ID" => $id), $bucketProducts, array());
				$allOk = self::bubbleError($r, $result);
			}
		}

		//Теперь все пустые наборы нужно удалить
		GiftCampaignGiftBucketTable::cleanupForCampaign($id);
	}


	/**
	 * Список кампаний с целями, подарками и наборами. Обязательно ID в select.
	 * @see \Bitrix\Main\Entity\DataManager::getList
	 *
	 * @param array $parameters параметры getList
	 *
	 * @return array
	 */
	public static function getListWithTargetsAndGiftsAndBuckets($parameters)
	{
		$campaigns = array();

		$rs = parent::getList($parameters);
		while ($campaign = $rs->fetch())
		{
			$campaigns[(int)$campaign["ID"]] = $campaign;
		}

		$ids = array_map(function ($c)
		{
			return (int)$c["ID"];
		}, $campaigns);
		if (!$ids)
		{
			$ids = array(-1);
		}

		//Подарки
		$rs = GiftCampaignGiftProductTable::getList(array(
			"select" => array("*"),
			"filter" => array("@CAMPAIGN_ID" => $ids)
		));
		$giftsMap = array();
		while ($gift = $rs->fetch())
		{
			$giftsMap[(int)$gift["CAMPAIGN_ID"]][] = $gift;
		}

		//Цели
		$rs = GiftCampaignTargetProductTable::getList(array(
			"select" => array("*"),
			"filter" => array("@CAMPAIGN_ID" => $ids)
		));
		$targetsMap = array();
		while ($target = $rs->fetch())
		{
			$targetsMap[(int)$target["CAMPAIGN_ID"]][] = $target;
		}

		//Наборы
		$rs = GiftCampaignGiftBucketProductTable::getList(array(
			"select" => array(
				"*",
				"CAMPAIGN_ID" => "BUCKET.CAMPAIGN_ID"
			),
			"filter" => array("@BUCKET.CAMPAIGN_ID" => $ids),
			"order" => array(
				"CAMPAIGN_ID" => "asc",
				"BUCKET_ID" => "ASC"
			)
		));
		$bucketProductsMap = array();
		while ($products = $rs->fetch())
		{
			$arTmp = array(
				"PRODUCT_ID" => $products["PRODUCT_ID"],
				"AMOUNT" => $products["AMOUNT"],
			);

			$bucketProductsMap[(int)$products["CAMPAIGN_ID"]][$products["BUCKET_ID"]][] = $arTmp;
		}

		foreach ($campaigns as $cid => &$campaign)
		{
			if (array_key_exists($cid, $giftsMap))
			{
				$campaign["GIFTS"] = $giftsMap[$cid];
			}
			else
			{
				$campaign["GIFTS"] = array();
			}

			if (array_key_exists($cid, $targetsMap))
			{
				$campaign["TARGETS"] = $targetsMap[$cid];
			}
			else
			{
				$campaign["TARGETS"] = array();
			}

			if (array_key_exists($cid, $bucketProductsMap))
			{
				$campaign["BUCKETS"] = $bucketProductsMap[$cid];
			}
			else
			{
				$campaign["BUCKETS"] = array();
			}
		}
		unset($campaign);

		return $campaigns;
	}

	/**
	 * Обработчик события после удаления кампании
	 * удаляет связанные с конкретной кампанией сущности: цели, подарки, наборы
	 *
	 * @param Event $event
	 */
	public static function onAfterDelete(Event $event)
	{
		$id = $event->getParameter("primary");
		$id = (int)$id["ID"];
		if ($id)
		{
			GiftCampaignTargetProductTable::deleteForCampaign($id);
			GiftCampaignGiftProductTable::deleteForCampaign($id);
			GiftCampaignGiftBucketTable::deleteForCampaign($id);
		}
	}
}
