<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Pavel Mashanov
 */

namespace Bit24\Promotions;

/**
 * Всякая сущность модуля
 * Class Entity
 * @package Bit24\Promotions
 */
abstract class Entity extends \Bitrix\Main\Entity\DataManager {

	/**
	 * Переносит ошибки из результата $r в  результат $result.
	 *
	 * @param \Bitrix\Main\Entity\Result $r
	 * @param \Bitrix\Main\Entity\Result $result
	 *
	 * @return bool has errors
	 */
	protected static function bubbleError($r, $result)
	{
		$allOk = true;
		if (!$r->isSuccess())
		{
			$allOk = false;
			foreach ($r->getErrorMessages() as $message)
			{
				$result->addError(new \Bitrix\Main\Entity\EntityError($message));
			}
		}

		return $allOk;
	}

}