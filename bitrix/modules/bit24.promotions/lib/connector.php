<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Pavel Mashanov
 */

namespace Bit24\Promotions;

use Bitrix\Main\EventManager;

/**
 * Коннектор модуля к каталогу и магазину
 *
 * @package Bit24\Promotions
 */
class Connector {

	static protected $basketEventHandlersEnabled = true;

	static protected $lastDeletedFUserId = null;

	/**
	 * Обработчки добавления в корзину
	 *
	 * @param int   $ID       ID строки
	 * @param array $arFields содержимое
	 */
	public static function OnBasketAdd($ID, $arFields)
	{
		if (static::$basketEventHandlersEnabled)
		{
			if ($arFields["FUSER_ID"])
			{
				GiftManager::applyCampaignsToBasket($arFields["FUSER_ID"]);
			}
		}
	}

	/**
	 * Обработчик обновления строки корзины
	 *
	 * @param int   $ID       ID строки
	 * @param array $arFields обновленыые поля
	 */
	public static function OnBasketUpdate($ID, $arFields)
	{
		if (static::$basketEventHandlersEnabled)
		{
			//При любых изменениях содержимого корзины применяем подарочные кампании

			$fuserId = $arFields["FUSER_ID"];
			if (!$fuserId)
			{
				$basketLine = static::getBasketLine($ID);
				$fuserId = $basketLine["FUSER_ID"];
			}

			if ($fuserId)
			{
				GiftManager::applyCampaignsToBasket($fuserId);
			}
		}
	}

	/**
	 * Обработчик события до удаления из корзины
	 *
	 * @param int $ID ID строки
	 */
	public static function OnBeforeBasketDelete($ID)
	{
		$basketLine = static::getBasketLine($ID);

		//При удлении из козины созраним fuser

		static::$lastDeletedFUserId = $basketLine["FUSER_ID"];
	}

	/**
	 * Обработчик события удаления из корзины
	 *
	 * @param int $ID ID строки
	 */
	public static function OnBasketDelete($ID)
	{
		if (static::$basketEventHandlersEnabled)
		{
			if (static::$lastDeletedFUserId)
			{
				//При любых изменениях содержимого корзины применяем подарочные кампании
				GiftManager::applyCampaignsToBasket(static::$lastDeletedFUserId);
			}
		}
	}

	//<editor-fold desc="Создание заказа. См. поток выполнения в bitrix/components/bethowen/sale.basket.order.ajax/component.php">
	/**
	 * Обработчик события до создания заказа
	 */
	public static function OnBeforeOrderAdd()
	{
		//Если мы добавили заказ [и сейчас начнем перепривязывать товары], то обработчики не нужны
		//Иначе как только будет "заказан" товар из целей, товарки соответствующей кампании исчезнут. Это не нужно.
		static::disableBasketHandlers();
	}

	/**
	 * Обработчик события после перенесения товаров из корзины в заказ
	 *
	 * @param int    $orderID     ID заказа
	 * @param int    $fuserID     ID покупателя
	 * @param string $strLang     язык
	 * @param array  $arDiscounts скидки
	 */
	public static function OnBasketOrder($orderID, $fuserID, $strLang, $arDiscounts)
	{
		if ($giftMessage = GiftManager::getOrderGiftsHumanDescription($orderID))
		{
			$arOrder = \CSaleOrder::GetByID($orderID);

			$newMessage = $arOrder["COMMENTS"] . $giftMessage;

			\CSaleOrder::Update($orderID, array("COMMENTS" => $newMessage));
		}

		//Когда все товары привязаны, включим обратно
		//Вдруг на одном хите происходит добавление заказа, добавление в корзину, опять добавление заказа и т.д.
		static::enableBasketHandlers();
	}

	//</editor-fold>

	/**
	 * Выключить обработчики корзины
	 */
	public static function disableBasketHandlers()
	{
		static::$basketEventHandlersEnabled = false;
	}

	/**
	 * Включить обработчкики корзины
	 */
	public static function enableBasketHandlers()
	{
		static::$basketEventHandlersEnabled = true;
	}

	/**
	 * Строка корзины по ID
	 *
	 * @param int $ID
	 *
	 * @return array
	 */
	protected static function getBasketLine($ID)
	{
		return \CSaleBasket::GetList(array(), array("ID" => $ID))->Fetch();
	}
}