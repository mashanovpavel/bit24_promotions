<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Pavel Mashanov
 */

namespace Bit24\Promotions\Helpers;

use Bit24\Promotions\GiftManager;

/**
 * Набор утилит, облегчающий работу с корзиной в компонентах публичной части
 * Class BasketHelper
 * @package Bit24\Promotions\Helpers
 */
class BasketHelper {

	/**
	 * Является ли строка корзины $arLine подарком
	 *
	 * @param array $arLine
	 *
	 * @return bool
	 */
	public static function isGiftBasketLine($arLine)
	{
		return array_key_exists("IS_GIFT", $arLine) && $arLine["IS_GIFT"];
	}

	/**
	 * Для содержимого корзины $basketContent заполняет map с кампаниями, если это подарок
	 *
	 * @param array $basketContent
	 */
	public static function fillGiftWithCampaignsForBasket(&$basketContent)
	{
		GiftManager::fillBasketLineToCampaignData($basketContent);

		$campaignIds = array();
		foreach ($basketContent as $basketLine)
		{
			if ($basketLine["GIFT_CAMPAIGN"])
			{
				$campaignIds[] = $basketLine["GIFT_CAMPAIGN"];
			}
		}

		$campaigns = array();
		if ($campaignIds)
		{
			$rs = \Bit24\Promotions\GiftCampaignTable::getList(array(
				"select" => array("*"),
				"filter" => array("@ID" => $campaignIds)
			));
			while ($arCampaign = $rs->fetch())
			{
				$campaigns[(int)$arCampaign["ID"]] = $arCampaign;
			}
		}

		foreach ($basketContent as &$basketLine)
		{
			if ($basketLine["IS_GIFT"])
			{
				$basketLine["GIFT_CAMPAIGN"] = $campaigns[$basketLine["GIFT_CAMPAIGN"]];
			}
		}
	}
}