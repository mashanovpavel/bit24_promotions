<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Pavel Mashanov
 */

namespace Bit24\Promotions;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\Loader;
use Bitrix\Main\Type\Collection;
use Bitrix\Main\Type\DateTime;

/**
 * Применяет подарочные акции к корзине
 * @package Bit24\Promotions\
 */
class GiftManager {

	const BASKET_GIFT_PROP_CODE = "BIT24.PROMOTIONS_GIFT_CAMPAIGN";
	const BASKET_BUCKET_PROP_CODE = "BIT24.PROMOTIONS_GIFT_BUCKET";
	const CATALOG_PRODUCT_PROVIDER = '\Bit24\Promotions\GiftProductProvider';
	const BASKET_GIFT_PROP_NAME = 'Подарок по акции';
	const BASKET_BUCKET_PROP_NAME = 'Подарок на выбор по акции';

	/**
	 * Применяет активные кампании к корзине пользователя $fuserId
	 *
	 * @param int $fuserId ID покупателя
	 */
	public static function applyCampaignsToBasket($fuserId)
	{
		$basketContent = static::getBasketContent($fuserId);
		$campaigns = static::getActiveGiftCampaigns();

		$giftBasketContent = static::getGiftBasketContent($basketContent, $campaigns);

		$updatePlan = static::makeUpdatePlan($fuserId, $basketContent, $giftBasketContent, $campaigns);

		static::applyPlan($updatePlan);
	}

	/**
	 * Возвращает содержимое корзины для покупателя $fuserId
	 * Если передан $fuserId - то содержимое заказа $orderId
	 *
	 * @param int    $fuserId ID покупателя
	 * @param string $orderId ID заказа.
	 *
	 * @return array
	 */
	protected static function getBasketContent($fuserId, $orderId = "NULL")
	{
		Loader::includeModule("sale");

		$basketContent = array();

		$arFilter = array(
			"ORDER_ID" => $orderId,
			"MODULE" => "catalog",
		);
		if ($fuserId)
		{
			$arFilter["FUSER_ID"] = $fuserId;
		}

		$rs = \CSaleBasket::GetList(array("ID" => "ASC"), $arFilter);
		while ($arBasketRow = $rs->Fetch())
		{
			$basketContent[$arBasketRow["ID"]] = $arBasketRow;
		}

		static::fillBasketLineToCampaignData($basketContent);

		return $basketContent;
	}

	/**
	 * Заполняет информацию о том, сколько из количества в корзине попало по подарочной акции и по какой
	 *
	 * @param array $basketContent содержимое корзины
	 */
	public static function fillBasketLineToCampaignData(&$basketContent)
	{
		$basketId2Index = array();
		foreach ($basketContent as $index => $basketLine)
		{
			$basketId2Index[(int)$basketLine["ID"]] = $index;
		}
		$basketIds = array_keys($basketId2Index);

		if (!is_array($basketIds) || !$basketIds)
		{
			return;
		}

		$properties = array();

		$arBasketPropsFilter = array(
			"@BASKET_ID" => $basketIds,
			"CODE" => array(
				static::BASKET_GIFT_PROP_CODE,
				static::BASKET_BUCKET_PROP_CODE
			)
		);

		$rs = \CSaleBasket::GetPropsList(array("ID" => "ASC"), $arBasketPropsFilter);
		while ($arProperty = $rs->Fetch())
		{
			$properties[$arProperty["CODE"]][$arProperty["BASKET_ID"]] = static::unserializeBasketLinePropertyValue($arProperty["VALUE"]);
		}

		foreach ($basketContent as &$basketLine)
		{
			$basketLineId = (int)$basketLine["ID"];
			$basketLine["GIFT_CAMPAIGN"] = $properties[static::BASKET_GIFT_PROP_CODE][$basketLineId] ? $properties[static::BASKET_GIFT_PROP_CODE][$basketLineId] : 0;

			$basketLine["GIFT_BUCKET"] = $properties[static::BASKET_BUCKET_PROP_CODE][$basketLineId] ? $properties[static::BASKET_BUCKET_PROP_CODE][$basketLineId] : 0;

			$basketLine["IS_GIFT"] = $basketLine["GIFT_CAMPAIGN"] || $basketLine["GIFT_BUCKET"];
		}
		unset($basketLine);
	}

	/**
	 * Десериализует значение свойства товара в корзине
	 *
	 * @param string $serialized
	 *
	 * @return int|null
	 */
	protected static function unserializeBasketLinePropertyValue($serialized)
	{
		$serialized = (int)$serialized;

		return $serialized ? $serialized : null;
	}

	/**
	 * Сериализует значение свойства товара в корзине
	 *
	 * @param mixed $value
	 *
	 * @return int|null
	 */
	protected static function serializeBasketLinePropertyValue($value)
	{
		return $value;
	}

	/**
	 * Возвращает действующие в данный момент подарочные кампании
	 * @return array
	 */
	protected static function getActiveGiftCampaigns()
	{
		$now = DateTime::createFromUserTime("");

		$filter = array(
			"=ACTIVE" => "Y",
			array(
				'LOGIC' => "OR",
				array(
					"=ACTIVE_FROM" => null,
					"=ACTIVE_TO" => null,
				),
				array(
					"!ACTIVE_FROM" => null,
					"<=ACTIVE_FROM" => $now,
					"=ACTIVE_TO" => null,
				),
				array(
					"=ACTIVE_FROM" => null,
					"!ACTIVE_TO" => null,
					">=ACTIVE_TO" => $now,
				),
				array(
					"!ACTIVE_FROM" => null,
					"!ACTIVE_TO" => null,
					"<=ACTIVE_FROM" => $now,
					">=ACTIVE_TO" => $now,
				),
			)
		);

		$parameters = array(
			"select" => array("*"),
			"filter" => $filter,
			"order" => array("ID" => "ASC")
		);

		return GiftCampaignTable::getListWithTargetsAndGiftsAndBuckets($parameters);
	}

	/**
	 * Существующие или планируемые к добавлению строки корзины, после применения кампаний
	 * Только некоторые ключи: QUANTITY, NATURAL_QUANTITY, GIFT_CAMPAIGNS_QUANTITY, GIFT_CAMPAIGNS
	 *
	 * @param array $basketContent содержимое корзины
	 * @param array $campaigns     кампании к применению
	 *
	 * @return array $resultBasketLine
	 */
	protected static function getGiftBasketContent($basketContent, $campaigns)
	{
		$campaign2SatisfyRatio = array();
		foreach ($campaigns as $campaign)
		{
			if (($ratio = static::getCampaignSatisfyRatio($campaign, $basketContent)) > 0)
			{
				$campaign2SatisfyRatio[(int)$campaign["ID"]] = $ratio;
			}
		}

		$product2basketLine = array();
		foreach ($basketContent as $basketLine)
		{
			$product2basketLine[(int)$basketLine["PRODUCT_ID"]] = (int)$basketLine["ID"];
		}

		//Корзина после применения акций, заготовка - без подарков
		$resultBasketLines = array();

		//Обычные подарки
		self::getBasketContentWithRegularGifts($basketContent, $campaigns, $campaign2SatisfyRatio, $resultBasketLines);

		//Из наборов
		self::getBasketContentWithBucketGifts($basketContent, $campaigns, $campaign2SatisfyRatio, $resultBasketLines);

		return $resultBasketLines;
	}

	/**
	 * Создает план апдейта - список действий над корзиной, проводящих корзину к состоянию со сработавшими акциями
	 *
	 * @param array $basketContent
	 * @param array $giftBasketContent
	 * @param array $campaigns
	 *
	 * @return array
	 */
	protected static function makeUpdatePlan($fuser, $basketContent, $giftBasketContent, $campaigns)
	{
		$updatePlan = array();

		/**
		 * Что вобще можно сделать со строкой?
		 * Обновить количество/описание
		 * Добавить
		 * Удалить
		 */

		//Добавить новые
		foreach ($giftBasketContent as $id => $new)
		{
			if (strpos($id, "NEW_GIFT_") === 0)
			{
				$campaign = $campaigns[$new["GIFT_CAMPAIGN"]];
				if ($planRow = static::makePlanAddAction($id, $new, $campaign, $fuser))
				{
					$updatePlan[] = $planRow;
				}
			}
		}

		//Обновить имеющиеся
		foreach ($giftBasketContent as $id => $new)
		{
			if (strpos($id, "NEW_GIFT_") === false)
			{
				$old = $basketContent[$id];
				if ($new["QUANTITY"] != $old["QUANTITY"])
				{
					$campaign = $campaigns[$new["GIFT_CAMPAIGN"]];
					if ($planRow = static::makePlanUpdateAction($id, $new, $basketContent[$id], $campaign))
					{
						$updatePlan[] = $planRow;
					}
				}
			}
		}

		//Удалить лишние
		foreach ($basketContent as $id => $old)
		{
			if ($old["IS_GIFT"])
			{
				if (!array_key_exists($id, $giftBasketContent))
				{
					if ($planRow = static::makePlanDeleteAction($id))
					{
						$updatePlan[] = $planRow;
					}
				}
			}
		}

		return $updatePlan;
	}

	/**
	 * Возвращает информацию о товаре $productId
	 *
	 * @param int $productId ID товара
	 *
	 * @return array
	 */
	protected static function getProductData($productId)
	{
		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"XML_ID",
			"NAME",
			"DETAIL_PAGE_URL"
		);
		$rs = \CIBlockElement::GetList(array(), array("=ID" => $productId), false, false, $arSelect);
		if ($arElement = $rs->GetNext())
		{
			$arIblock = \CIBlock::GetList(array(), array(
				"ID" => $arElement["IBLOCK_ID"],
				false
			))->Fetch();
		}

		return array(
			$arIblock,
			$arElement
		);
	}

	/**
	 * Возвращает данные для дейсвия "Добавить товар в корзину" плана апдейта
	 *
	 * @param int   $rowId    ID строки корзины
	 * @param array $new      новая версия этой строки корзины
	 * @param array $campaign кампания, по которой строка попала в корзину
	 * @param int   $fuser    ID покупателя
	 *
	 * @return array элемент плана апдейта корзины
	 */
	protected static function makePlanAddAction($rowId, $new, $campaign, $fuser)
	{
		$arProduct = ElementTable::getById($new["PRODUCT_ID"])->fetch();
		$arPrice = \CPrice::GetBasePrice($arProduct["ID"]);

		list($arIblock, $arElement) = static::getProductData($arProduct["ID"]);

		$arProps = array();
		if ($arIblock["XML_ID"] != '')
		{
			$arProps[] = array(
				"NAME" => "Catalog XML_ID",
				"CODE" => "CATALOG.XML_ID",
				"VALUE" => $arIblock["XML_ID"]
			);
		}
		$arProps[] = static::getBasketRowPropertyForValue($new["GIFT_CAMPAIGN"]);

		$arBasketAdd = array(
			'CUSTOM_PRICE' => 'Y',
			'IGNORE_CALLBACK_FUNC' => 'Y',
			'CAN_BUY' => 'Y',

			'PRODUCT_PROVIDER_CLASS' => '\Bit24\Promotions\GiftProductProvider',

			'PRODUCT_ID' => $arProduct["ID"],
			'PRODUCT_PRICE_ID' => $arPrice["ID"],
			'PRICE' => '0',
			'CURRENCY' => $arPrice["CURRENCY"],
			'WEIGHT' => $arProduct['PRODUCT_WEIGHT'],
			'QUANTITY' => $new["QUANTITY"],
			'LID' => $arIblock["LID"],
			'NAME' => $arProduct['NAME'],
			'PRODUCT_XML_ID' => $arElement["XML_ID"],
			'CATALOG_XML_ID' => $arIblock["XML_ID"],
			'MODULE' => "catalog",
			'DETAIL_PAGE_URL' => $arElement['DETAIL_PAGE_URL'],
			'FUSER_ID' => $fuser,

			'DISCOUNT_PRICE' => $arPrice['PRICE'],
			'DISCOUNT_NAME' => "Подарок по акции " . $campaign["NAME"],
			'DISCOUNT_VALUE' => '100',
			'PROPS' => $arProps
		);

		return array(
			"ACTION" => "ADD",
			"PAYLOAD" => $arBasketAdd
		);
	}

	/**
	 * Возвращает данные для дейсвия "Обновить товар в корзине" плана апдейта
	 *
	 * @param int   $rowId    ID строки корзины
	 * @param array $new      новая версия этой строки корзины
	 * @param array $old      прежняя версия этой строки корзины
	 * @param array $campaign кампания, по которой строка попала в корзину
	 *
	 * @return array элемент плана апдейта корзины
	 */
	protected static function makePlanUpdateAction($rowId, $new, $old, $campaign)
	{
		$arBasketUpdate = array(
			"QUANTITY" => $new["QUANTITY"],
			"DISCOUNT_NAME" => $campaign,
			"CAN_BUY" => "Y"
			//на время борьбы с предыдущим модулем
		);

		return array(
			"ACTION" => "UPDATE",
			"BASKET_ID" => $rowId,
			"PAYLOAD" => $arBasketUpdate,
		);
	}

	/**
	 * Возвращает данные для дейсвия "Удалить товар из корзины" плана апдейта
	 *
	 * @param int $rowId ID строки корзины
	 *
	 * @return array элемент плана апдейта корзины
	 */
	protected static function makePlanDeleteAction($rowId)
	{
		return array(
			"ACTION" => "DELETE",
			"BASKET_ID" => $rowId
		);
	}

	/**
	 * Исполняет план обновления корзины  $updatePlan
	 * @note На время обновления отключает обработчики событий корзины
	 * @see  Connector::disableBasketHandlers
	 * @see  Connector::enableBasketHandlers
	 *
	 * @param array $updatePlan
	 */
	protected static function applyPlan($updatePlan)
	{
		Connector::disableBasketHandlers();
		foreach ($updatePlan as $planRow)
		{
			if ($planRow["ACTION"] == "ADD")
			{
				\CSaleBasket::Add($planRow["PAYLOAD"]);
			}
			elseif ($planRow["ACTION"] == "UPDATE")
			{
				\CSaleBasket::Update($planRow["BASKET_ID"], $planRow["PAYLOAD"]);
			}
			elseif ($planRow["ACTION"] == "DELETE")
			{
				\CSaleBasket::Delete($planRow["BASKET_ID"]);
			}
		}
		Connector::enableBasketHandlers();
	}

	/**
	 * Получает ссвойства товара в корзине для строки корзины $basketLineId
	 *
	 * @param int $basketLineId ID строки корзины
	 *
	 * @return array
	 */
	protected static function getBasketPropertiesForLine($basketLineId)
	{
		$rows = array();

		$rs = \CSaleBasket::GetPropsList(array(), array("BASKET_ID" => $basketLineId));
		while ($arPropRow = $rs->Fetch())
		{
			$rows[$arPropRow["CODE"]] = $arPropRow;
		}

		return $rows;
	}

	/**
	 * Возвращает массив для установки свойства товара в корзине
	 *
	 * @param array $giftCampaignsValue значение свойства
	 *
	 * @return array
	 */
	protected static function getBasketRowPropertyForValue($giftCampaignsValue)
	{
		return array(
			"CODE" => static::BASKET_GIFT_PROP_CODE,
			"VALUE" => static::serializeBasketLinePropertyValue($giftCampaignsValue),
			"SORT" => 10000,
			"NAME" => static::BASKET_GIFT_PROP_NAME
		);
	}

	/**
	 * Сколько раз кампания нашла цели в корзине.
	 * Для не множественных кампаниий - всегда не больше 1.
	 *
	 * @param array $campaign      кампания
	 * @param array $basketContent содержимое корзины
	 *
	 * @return int $ratio
	 */
	protected static function getCampaignSatisfyRatio($campaign, $basketContent)
	{
		//Кампания работающая без товаров-целей применит подарки единожды
		if (count($campaign["TARGETS"]) === 0)
		{
			return 1;
		}

		$target2amount = array();
		foreach ($campaign["TARGETS"] as $target)
		{
			$target2amount[$target["PRODUCT_ID"]] = $target["AMOUNT"];
		}

		$product2quantity = array();
		foreach ($basketContent as $basketLine)
		{
			if (!$basketLine["IS_GIFT"])
			{
				$product2quantity[$basketLine["PRODUCT_ID"]] = $basketLine["QUANTITY"];
			}
		}

		//Сколько раз найдено нужное количество каждого товара
		$product2satisfyRatio = array();
		foreach ($target2amount as $productId => $targetAmount)
		{
			if (array_key_exists($productId, $product2quantity))
			{
				//При неправильно заполенной базе предотвратит деление на ноль
				if ($targetAmount <= 0)
				{
					$product2satisfyRatio[$productId] = 1;
				}
				else
				{
					$product2satisfyRatio[$productId] = (int)floor($product2quantity[$productId] / $targetAmount);
				}
			}
			else
			{
				$product2satisfyRatio[$productId] = 0;
				break;
			}
		}

		//Наименее найденый товар
		reset($product2satisfyRatio);
		$minRatio = current($product2satisfyRatio);
		foreach ($product2satisfyRatio as $ratio)
		{
			if ($ratio < $minRatio)
			{
				$minRatio = $ratio;
			}
		}

		//Для не множественной кампании ratio не выше 1
		$isMultipleApplyAllowed = $campaign["MULTIPLE_APPLY"] == "Y";
		if (!$isMultipleApplyAllowed && $minRatio > 1)
		{
			$minRatio = 1;
		}

		return $minRatio;
	}

	/**
	 * Является ли строка корзины подарком
	 *
	 * @param int $ID ID строки корзизы
	 *
	 * @return bool
	 */
	public static function isGiftBasketLine($ID)
	{
		$rs = \CSaleBasket::GetPropsList(array(), array(
			"BASKET_ID" => $ID,
			"CODE" => static::BASKET_GIFT_PROP_CODE
		));
		if ($arPropRow = $rs->Fetch())
		{
			return (int)$arPropRow["VALUE"] > 0;
		}

		return false;
	}

	/**
	 * Возвращает человеческое описание списка подарков заказа $orderId
	 *
	 * @param int $orderId ID заказа
	 *
	 * @return string
	 */
	public static function getOrderGiftsHumanDescription($orderId)
	{
		$basketContent = static::getBasketContent(0, $orderId);

		$basketContentWithGifts = array();
		$campaignIds = array();

		foreach ($basketContent as $basketLine)
		{
			if ($basketLine["IS_GIFT"])
			{
				$basketContentWithGifts[] = $basketLine;

				$campaignIds[] = $basketLine["GIFT_CAMPAIGN"];
			}
		}

		$campaigns = array();
		if ($campaignIds)
		{
			$rs = GiftCampaignTable::getList(array(
				"select" => array("*"),
				"filter" => array("@ID" => $campaignIds)
			));
			while ($arCampaign = $rs->fetch())
			{
				$campaigns[(int)$arCampaign["ID"]] = $arCampaign;
			}
		}

		$giftsArray = array();
		$totalGiftsQuantity = 0;
		if ($basketContentWithGifts)
		{
			foreach ($basketContentWithGifts as $basketLine)
			{
				$campaignId = $basketLine["GIFT_CAMPAIGN"];
				$campaignName = $campaigns[$basketLine["GIFT_CAMPAIGN"]];
				$quantity = (int)$basketLine["QUANTITY"];
				$productName = $basketLine["NAME"];
				$productId = $basketLine["PRODUCT_ID"];

				$totalGiftsQuantity += $quantity;

				$giftsArray[] = "По кампании #$campaignId $quantity шт. [$productId] '$productName'";
			}

			$header = "\n----Подарки----\n";
			$footer = "\n---------------\n";

			return $header . implode("\n", $giftsArray) . $footer;
		}

		return "";
	}

	/**
	 * Заполняет новое содержимое корзины обычными подарками активных акций
	 *
	 * @param array $basketContent содержимое корзины
	 * @param array $campaigns кампании к применению
	 * @param array $campaign2SatisfyRatio мап удовлетворенности кампаний
	 * @param array $resultBasketLines новое содержимое корзины
	 */
	protected static function getBasketContentWithRegularGifts($basketContent, $campaigns, $campaign2SatisfyRatio, &$resultBasketLines)
	{
		foreach ($campaign2SatisfyRatio as $campaignId => $ratio)
		{
			$campaign = $campaigns[$campaignId];

			$giftsproduct2amount = array();
			foreach ($campaign["GIFTS"] as $gift)
			{
				$giftsproduct2amount[(int)$gift["PRODUCT_ID"]] = (int)$gift["AMOUNT"];
			}

			$giftsInBasketProduct2Line = array();
			foreach ($basketContent as $id => $basketLine)
			{
				if ($basketLine["IS_GIFT"] && $basketLine["GIFT_CAMPAIGN"] == $campaignId)
				{
					$giftsInBasketProduct2Line[(int)$basketLine["PRODUCT_ID"]] = $id;
				}
			}

			foreach ($giftsproduct2amount as $productId => $amount)
			{
				if (array_key_exists($productId, $giftsInBasketProduct2Line))
				{
					//Подарок уже есть в корзине
					$basketLineId = $giftsInBasketProduct2Line[$productId];
					$basketLine = $basketContent[$basketLineId];

					$resultBasketLines[$basketLineId] = $basketLine;
					$resultBasketLines[$basketLineId]["QUANTITY"] = $ratio * $amount;
				}
				else
				{
					//Нет, добавить
					$basketLineId = "NEW_GIFT_" . $campaignId . "_" . $productId;

					$resultBasketLines[$basketLineId] = array(
						"PRODUCT_ID" => $productId,
						"QUANTITY" => $ratio * $amount,
						"IS_GIFT" => true,
						"GIFT_CAMPAIGN" => $campaignId,
					);
				}
			}
		}

		//Если в корзине только подарки (акция(и) без целей) - отнять
		//Такие подарки только в комплекте с нормальным товаром за деньги
		$onlyGifts = true;
		foreach ($basketContent as $basketLine)
		{
			if (!$basketLine["IS_GIFT"])
			{
				$onlyGifts = false;
				break;
			}
		}
		if ($onlyGifts)
		{
			$resultBasketLines = array();
		}
	}

	/**
	 * Заполняет новое содержимое корзины подарками из выбранных пользователем наборов активных акций
	 *
	 * @param array $basketContent содержимое корзины
	 * @param array $campaigns кампании к применению
	 * @param array $campaign2SatisfyRatio мап удовлетворенности кампаний
	 * @param array $resultBasketLines новое содержимое корзины
	 */
	protected static function getBasketContentWithBucketGifts($basketContent, $campaigns, $campaign2SatisfyRatio, &$resultBasketLines)
	{
		$satisfactedCapmaignIds = array_keys($campaign2SatisfyRatio);

		if(!$satisfactedCapmaignIds)
			return;

		//Кампании с наборами
		$campaignsWithBuckets = array();
		foreach($satisfactedCapmaignIds as $campaignId)
		{
			if(count($campaigns[$campaignId]["BUCKETS"]) > 0)
			{
				$campaignsWithBuckets[$campaignId] = $campaigns[$campaignId];
			}
		}

		//Получить выбранные пользователем наборы для этого набора кампаний
		$bucketsInBasket = array();



		//Оставить по одному для каждой кампании


		//Насыпать или обновить в корзину
	}
}