<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Pavel Mashanov
 */
namespace Bit24\Promotions\UI;

use Bitrix\Iblock\ElementTable;

\CJSCore::RegisterExt("listoflinkedproducts", array(
	"js" => "/bitrix/js/bit24.promotions/ui_listoflinkedproducts.js",
	"css" => "/bitrix/themes/.default/bit24.promotions.css",
	"rel" => array(
		"jquery",
		"linkedproducts",
		"md5"
	)
));

/**
 * Список списков товаров с количеством
 * Class ListOfLinkedProducts
 * @package Bit24\Promotions\UI
 */
class ListOfLinkedProducts {

	protected $params = array();
	protected $name = "";
	/** @var array BUCKET_ID => array(array(PRODUCT_ID => 1, AMOUNT => 2),...) */
	protected $values = array();

	protected static function escapeFunctionName($fname)
	{
		return str_replace(array(
			"[",
			"]"
		), array(
			"_",
			"_"
		), $fname);
	}

	/**
	 * @param string $name
	 * @param string $values
	 */
	public function __construct($name, $values)
	{
		$this->name = $name;
		$this->values = $values;
	}

	/**
	 * @param array|string $values array(BUCKET_ID => array(array(PRODUCT_ID => AMOUNT),...))
	 */
	public function setValues($values)
	{
		$this->values = $values;
	}

	public function show()
	{
		\CJSCore::Init("listoflinkedproducts");

		$propName = $this->name;
		$escapedPropName = self::escapeFunctionName($propName);
		?>
		<div class="product-list-wrapper">
			<table id="<?= $escapedPropName ?>-listoflinkedproductstable" class="listoflinkedproductstable-table">
				<tbody>
				<?
				$index = 0;
				?>
				<? foreach ($this->values as $bucketId => $products):
					static::printRow($index, $propName, $bucketId, $products, " ");
					$index++;
				endforeach ?>

				<? static::printRow("#INDEX#", $propName, "", array(), " style=\"display: none;\"", "js-listoflinkedproducts-row-template"); ?>
				</tbody>
			</table>
			<input type="button" value="Добавить набор" class="js-add-row-<?= $escapedPropName ?>">
			<script type="text/javascript">
				$(document).ready(function() {
					new ListOfLinkedProducts(<?=json_encode($this->name)?>);
				});
			</script>
		</div>
		<?
	}

	protected static function printRow($index, $propName, $bucketId, $products, $style = "", $class = "js-list-row")
	{
		?>
		<tr class="<?= $class ?>" data-basenumber="<?= $index ?>" <?= $style ?>>
			<td class="js-id-cell">
				ID: <span><?= $bucketId ?></span>
			</td>
			<td class="js-panel-cell">
				<?
				$nameForLinkedProducts = $propName . "[$bucketId]";
				$linkedProducts = new LinkedProducts($nameForLinkedProducts, $index);
				$linkedProducts->setValues($products);
				$linkedProducts->show();
				?>
			</
			>
			<td class="js-button-cell"><input type="button" class="js-delete-listoflinkedproducts-row-button"
			                                  value="x"/></td>
		</tr>
		<?
	}

	public function parseForm($request)
	{
		$map = array();

		if (array_key_exists($this->name, $request))
		{
			foreach ($request[$this->name] as $bucketId => $currentBucket)
			{
				if (array_key_exists("PRODUCT", $currentBucket) && array_key_exists("AMOUNT", $currentBucket))
				{
					foreach ($currentBucket["PRODUCT"] as $key => $value)
					{
						$product = (int)$value;
						$amount = (int)$currentBucket["AMOUNT"][$key];

						if ($product && $amount)
						{
							$map[$bucketId][$product] = $amount;
						}
					}
				}
			}
		}

		$result = array();

		foreach ($map as $bucketId => $content)
		{
			foreach ($content as $product => $amount)
			{
				$result[$bucketId][] = array(
					"PRODUCT_ID" => $product,
					"AMOUNT" => $amount,
				);
			}
		}

		return $result;
	}
}