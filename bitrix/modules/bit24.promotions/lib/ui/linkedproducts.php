<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Pavel Mashanov
 */
namespace Bit24\Promotions\UI;

use Bitrix\Iblock\ElementTable;

\CJSCore::RegisterExt("linkedproducts", array(
	"js" => "/bitrix/js/bit24.promotions/ui_linkedproducts.js",
	"css" => "/bitrix/themes/.default/bit24.promotions.css",
	"rel" => array(
		"jquery",
		"md5"
	)
));

/**
 * Список товаров с количеством
 *
 * Class LinkedProducts
 * @package Bit24\Promotions\UI
 */
class LinkedProducts {

	protected $params = array();
	protected $name = "";
	protected $values = array();

	protected static function escapeFunctionName($fname)
	{
		return str_replace(array(
			"[",
			"]"
		), array(
			"_",
			"_"
		), $fname);
	}

	/**
	 * @param string $name
	 * @param string $values
	 */
	public function __construct($name, $values)
	{
		$this->name = $name;
		$this->values = $values;
	}

	/**
	 * @param array|string $values array(PRODUCT_ID => AMOUNT, )
	 */
	public function setValues($values)
	{
		$cleared = array();

		foreach ($values as $giftsRow)
		{
			$cleared[(int)$giftsRow["PRODUCT_ID"]] = (int)$giftsRow["AMOUNT"];
		}

		$this->values = $cleared;
	}

	protected static function getProductsData($values)
	{
		$products = array();
		$pids = array_keys($values);

		$filter = $pids ? array("@ID" => $pids) : array("=ID" => -1);

		$rs = ElementTable::getList(array(
			"select" => array(
				"ID",
				"IBLOCK_ID",
				"NAME"
			),
			"filter" => $filter
		));

		while ($row = $rs->fetch())
		{
			$row["ADMIN_URL"] = \CIBlock::GetAdminElementEditLink((int)$row["IBLOCK_ID"], (int)$row["ID"]);
			$products[(int)$row["ID"]] = $row;
		}

		return $products;
	}

	public function show()
	{
		\CJSCore::Init("linkedproducts");

		$productData = $this->getProductsData($this->values);
		$propName = $this->name;
		$escapedPropName = self::escapeFunctionName($propName);

		?>
		<div class="product-table-wrapper">
			<table id="<?= $escapedPropName ?>-linkedproductstable" class="linkedproductstable-table">
				<tbody>
				<? $index = 0;
				$iblockId = 0; ?>
				<? foreach ($this->values as $productId => $amount):

					if (array_key_exists($productId, $productData))
					{
						$productName = htmlspecialcharsbx($productData[$productId]["NAME"]);
						$iblockId = $productData[$productId]["IBLOCK_ID"];
					}
					else
					{
						$productName = "Не найден";
					}

					static::printRow($index, $propName, $productId, $amount, $productName, $iblockId, "", "js-product-row");

					$index++;
				endforeach ?>

				<? static::printRow("#INDEX#", $propName, "", 1, "", $iblockId, " style=\"display: none;\"", "js-product-row-template"); ?>
				</tbody>
			</table>
			<input type="button" value="Добавить товары" class="js-add-rows-<?= $escapedPropName ?>">
			<script type="text/javascript">
				$(document).ready(function() {
					new LinkedProducts(<?=json_encode($propName)?>, <?=json_encode(count($this->values))?>);
				});
			</script>
		</div>
		<?
	}

	protected static function printRow($index, $propName, $productId, $amount, $productName, $iblockId, $style = "", $class = "js-product-row")
	{
		?>
		<tr class="<?= $class ?>" data-basenumber="<?= $index ?>" <?= $style ?>>
			<td class="js-change-product-cell">
				<input type="text" name="<?= $propName ?>[PRODUCT][<?= $index ?>]" value="<?= $productId ?>"
				       id="<?= $propName ?>[PRODUCT][<?= $index ?>]" size="6"/>
			</td>
			<td>
				<input type="button" value="..." class="js-change-product-button">
			</td>
			<td class="js-product-cell" data-productId="<?= $productId ?>">
				<span id="sp_<?= md5($propName . "[PRODUCT]") ?>_<?= $index ?>"><?= $productName ?></span>
			</td>
			<td class="js-amount-cell"><input type="text" name="<?= $propName ?>[AMOUNT][<?= $index ?>]"
			                                  value="<?= $amount ?>" size="4"/></td>
			<td class="js-button-cell"><input type="button" class="js-delete-row-button" value="x"/></td>
		</tr>
		<?
	}

	public function parseForm($request)
	{
		$result = array();

		if (array_key_exists($this->name, $request))
		{
			foreach ($request[$this->name]["PRODUCT"] as $key => $value)
			{
				$product = (int)$value;
				$amount = (int)$request[$this->name]["AMOUNT"][$key];

				if ($product && $amount)
				{
					$result[] = array(
						"PRODUCT_ID" => $product,
						"AMOUNT" => $amount,
					);
				}
			}
		}

		return $result;
	}
}