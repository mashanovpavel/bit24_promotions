<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Pavel Mashanov
 */

namespace Bit24\Promotions;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\Event;
use Bitrix\Main\Entity\Result;
use Bitrix\Main\Localization\Loc;
use Bit24\Promotions\Entity as Own;

Loc::loadMessages(__FILE__);

/**
 * Набор подарков на выбор
 * Class GiftCampaignGiftProductTable
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> CAMPAIGN_ID int mandatory
 * </ul>
 * @package Bit24\Promotions
 **/
class GiftCampaignGiftBucketTable extends Own {

	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return 'bit24_promotions_gift_campaign_gift_bucket';
	}

	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('GIFT_CAMPAIGN_GIFT_BUCKET_ENTITY_ID_FIELD'),
			),
			'CAMPAIGN_ID' => array(
				'data_type' => 'integer',
				'validation' => array(
					__CLASS__,
					'validateCampaign'
				),
				'title' => Loc::getMessage('GIFT_CAMPAIGN_GIFT_PRODUCT_ENTITY_CAMPAIGN_FIELD'),
			),
			//отношения
			'CAMPAIGN' => new Entity\ReferenceField('CAMPAIGN', 'Bit24\Promotions\GiftCampaign', array('=this.CAMPAIGN_ID' => 'ref.ID')),
		);
	}

	/**
	 * Валидатор кампании
	 * @return array
	 */
	public static function validateCampaign()
	{
		return array(
			new Entity\Validator\Length(1, null, array("MIN" => "Не указана кампания")),
			new Entity\Validator\Range(1, null, false, array("MIN" => "Не указана кампания")),
		);
	}

	/**
	 * Сохраняет набор с товарами
	 * передача $existingProducts сохраняет запрос к базе
	 *
	 * @param int   $id               ID набора
	 * @param array $bucket           поля набора
	 * @param array $products         товары набора вида array(array("PRODUCT_ID" => int, "AMOUNT" => int),...)
	 * @param array $existingProducts продукты этого набора, уже извлеченные из базы
	 *
	 * @return Entity\AddResult|Entity\UpdateResult
	 */
	public static function saveWithProducts($id = 0, $bucket, $products, $existingProducts = null)
	{
		if ($id)
		{
			if (empty($bucket))
			{
				$bucket = array("ID" => $id);
			}
			$result = self::update($id, $bucket);
		}
		else
		{
			$result = self::add($bucket);
			$id = $result->getId();
		}

		if ($result->isSuccess())
		{
			self::__saveProducts($result, $id, $products, $existingProducts);
		}

		return $result;
	}

	/**
	 * Сохраняет товары в процессе сохранения набора
	 *
	 * @param Result $result           результат всей операции
	 * @param int    $id               ID набора
	 * @param array  $products         товары набора вида array(array("PRODUCT_ID" => int, "AMOUNT" => int),...)
	 * @param null   $existingProducts продукты этого набора, уже извлеченные из базы
	 */
	protected static function __saveProducts($result, $id, $products, $existingProducts = null)
	{
		if ($existingProducts === null)
		{
			$existingProducts = GiftCampaignGiftBucketProductTable::getForBucket($id);
		}

		$existingProduct2Amount = array();
		foreach ($existingProducts as $p)
		{
			$existingProduct2Amount[$p["PRODUCT_ID"]] = $p["AMOUNT"];
		}

		$newProduct2Amount = array();
		foreach ($products as $p)
		{
			$newProduct2Amount[$p["PRODUCT_ID"]] = $p["AMOUNT"];
		}

		$add = array();
		$update = array();
		foreach ($newProduct2Amount as $productId => $amount)
		{
			if (array_key_exists($productId, $existingProduct2Amount))
			{
				if ($amount != $existingProduct2Amount[$productId])
				{
					$update[$productId] = $amount;
				}
			}
			else
			{
				$add[$productId] = $amount;
			}
		}

		$delete = array_diff_key($existingProduct2Amount, $newProduct2Amount);

		//Исполнение плана
		$allOk = true;
		if ($allOk)
		{
			foreach (array_keys($delete) as $productId)
			{
				$r = GiftCampaignGiftBucketProductTable::delete(array(
					"BUCKET_ID" => $id,
					"PRODUCT_ID" => $productId
				));
				$allOk = self::bubbleError($r, $result);
			}
		}
		if ($allOk)
		{
			foreach ($update as $productId => $amount)
			{
				$r = GiftCampaignGiftBucketProductTable::update(array(
					"BUCKET_ID" => $id,
					"PRODUCT_ID" => $productId
				), array("AMOUNT" => $amount));
				$allOk = self::bubbleError($r, $result);
			}
		}
		if ($allOk)
		{
			foreach ($add as $productId => $amount)
			{
				$r = GiftCampaignGiftBucketProductTable::add(array(
					"BUCKET_ID" => $id,
					"PRODUCT_ID" => $productId,
					"AMOUNT" => $amount
				));
				$allOk = self::bubbleError($r, $result);
			}
		}
	}


	/**
	 * Наборы для кампании $campaignId
	 *
	 * @param int $campaignId ID кампании
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getForCampaign($campaignId)
	{
		$select = array("*");

		return self::getList(array(
			"select" => $select,
			"filter" => array("=CAMPAIGN_ID" => $campaignId)
		))->fetchAll();
	}

	public static function deleteForCampaign($campaignId)
	{
		$cursor = self::getList(array(
			"select" => array(
				"ID",
			),
			"filter" => array("=CAMPAIGN_ID" => $campaignId)
		));

		while ($row = $cursor->fetch())
		{
			static::delete($row);
		}
	}

	/**
	 * Удаляет все наборы кампании, не имеющие товаров
	 *
	 * @param int $campaignId ID кампании
	 */
	public static function cleanupForCampaign($campaignId)
	{
		//Можно в один запрос с подзапросом, но время...
		//Получим наборы с товарами
		$existingProducts = GiftCampaignGiftBucketProductTable::getForCampaign($campaignId);
		$bucketWithProductsIds = array_keys($existingProducts);

		//Получим все
		$existingBuckets = GiftCampaignGiftBucketTable::getForCampaign($campaignId);
		$allBucketsIds = array_map(function ($bucket)
		{
			return $bucket["ID"];
		}, $existingBuckets);

		//Разницу удалим
		foreach (array_diff($allBucketsIds, $bucketWithProductsIds) as $bucketId)
		{
			GiftCampaignGiftBucketTable::delete($bucketId);
		}
	}

	/**
	 * Обработчик после удаления набора
	 * Удаляет связанные с набором сущности: товары в наборе
	 *
	 * @param Event $event
	 */
	public static function onAfterDelete(Event $event)
	{
		$id = $event->getParameter("primary");
		$id = (int)$id["ID"];
		if ($id)
		{
			GiftCampaignGiftBucketProductTable::deleteForBucket($id);
		}
	}
}
