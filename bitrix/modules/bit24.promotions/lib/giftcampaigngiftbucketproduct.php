<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Pavel Mashanov
 */

namespace Bit24\Promotions;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bit24\Promotions\Entity as Own;

Loc::loadMessages(__FILE__);

/**
 * Товар с количеством в наборе
 * Class GiftCampaignGiftBucketProductTable
 * Fields:
 * <ul>
 * <li> CAMPAIGN_ID int mandatory
 * <li> PRODUCT_ID int mandatory
 * <li> AMOUNT int optional default 1
 * </ul>
 * @package Bit24\Promotions
 **/
class GiftCampaignGiftBucketProductTable extends Own {

	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return 'bit24_promotions_gift_campaign_gift_bucket_product';
	}

	public static function getMap()
	{
		return array(
			'BUCKET_ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'validation' => array(
					__CLASS__,
					'validateBucket'
				),
				'title' => Loc::getMessage('GIFT_CAMPAIGN_GIFT_PRODUCT_ENTITY_CAMPAIGN_FIELD'),
			),
			'PRODUCT_ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'validation' => array(
					__CLASS__,
					'validateProduct'
				),
				'title' => Loc::getMessage('GIFT_CAMPAIGN_GIFT_PRODUCT_ENTITY_PRODUCT_FIELD'),
			),
			'AMOUNT' => array(
				'data_type' => 'integer',
				'validation' => array(
					__CLASS__,
					'validateAmount'
				),
				'title' => Loc::getMessage('GIFT_CAMPAIGN_GIFT_PRODUCT_ENTITY_AMOUNT_FIELD'),
			),
			//отношения
			'CAMPAIGN_ID' => new Entity\ReferenceField('CAMPAIGN_ID', 'Bit24\Promotions\GiftCampaignGiftBucket:BUCKET.CAMPAIGN_ID', array('=this.BUCKET_ID' => 'ref.ID')),
			//			'CAMPAIGN' => new Entity\ReferenceField('CAMPAIGN', 'Bit24\Promotions\GiftCampaignGiftBucket:BUCKET.CAMPAIGN', array('=this.BUCKET_ID' => 'ref.ID')),
			'BUCKET' => new Entity\ReferenceField('BUCKET', 'Bit24\Promotions\GiftCampaignGiftBucket', array('=this.BUCKET_ID' => 'ref.ID')),
			new Entity\ReferenceField('PRODUCT', 'Bitrix\Sale\Product', array('=this.PRODUCT_ID' => 'ref.ID')),
		);
	}

	/**
	 * Валидатор количества
	 * @return Entity\IValidator[]
	 */
	public static function validateAmount()
	{
		return array(
			new Entity\Validator\Length(1, null, array("MIN" => "Не указано количество")),
			new Entity\Validator\Range(1, null, false, array("MIN" => "Количество должно быть больше нуля")),
		);
	}

	/**
	 * Валидатор товара
	 * @return Entity\IValidator[]
	 */
	public static function validateProduct()
	{
		return array(
			new Entity\Validator\Length(1, null, array("MIN" => "Не указан товар")),
			new Entity\Validator\Range(1, null, false, array("MIN" => "Некорректный товар")),
		);
	}

	/**
	 * Валидатор набора
	 * @return Entity\IValidator[]
	 */
	public static function validateBucket()
	{
		return array(
			new Entity\Validator\Length(1, null, array("MIN" => "Не указана кампания")),
			new Entity\Validator\Range(1, null, false, array("MIN" => "Не указана кампания")),
		);
	}

	/**
	 * Возвращает все товары всех наборов кампании $campaignId
	 *
	 * @param int  $campaignId      ID кампании
	 * @param bool $withExtraFields возвращать название и ID инфоблока товара
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getForCampaign($campaignId, $withExtraFields = false)
	{
		$select = array("*");
		if ($withExtraFields)
		{
			$select = array_merge($select, array(
				"NAME" => "PRODUCT.IBLOCK.NAME",
				"IBLOCK_ID" => "PRODUCT.IBLOCK.IBLOCK_ID"
			));
		}

		$select = array_merge($select, array("CAMPAIGN_ID" => 'BUCKET.CAMPAIGN_ID'));

		$rs = self::getList(array(
			"select" => $select,
			"filter" => array('BUCKET.CAMPAIGN_ID' => $campaignId)
		));

		$map = array();
		while ($arProductRow = $rs->fetch())
		{
			$campaignId = (int)$arProductRow["CAMPAIGN_ID"];
			$bucketId = (int)$arProductRow["BUCKET_ID"];
			$productId = (int)$arProductRow["PRODUCT_ID"];

			$map[$campaignId][$bucketId][$productId] = $arProductRow;
		}

		return $map[$campaignId] ? $map[$campaignId] : array();
	}

	/**
	 * Все товары для набора $bucketId
	 *
	 * @param int $bucketId ID набора
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getForBucket($bucketId)
	{
		$select = array("*");

		return self::getList(array(
			"select" => $select,
			"filter" => array("=BUCKET_ID" => $bucketId)
		))->fetchAll();
	}

	/**
	 * Удаляет все товары для набора  $bucketId
	 *
	 * @param int $bucketId ID набора
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function deleteForBucket($bucketId)
	{
		$cursor = self::getList(array(
			"select" => array(
				"BUCKET_ID",
				"PRODUCT_ID"
			),
			"filter" => array("=BUCKET_ID" => $bucketId)
		));

		while ($row = $cursor->fetch())
		{
			static::delete($row);
		}
	}
}
