<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Pavel Mashanov
 */

namespace Bit24\Promotions;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bit24\Promotions\Entity as Own;

Loc::loadMessages(__FILE__);

/**
 * Товары, по наличию которых срабатывает кампания (целевые)
 * Fields:
 * <ul>
 * <li> CAMPAIGN_ID int mandatory
 * <li> PRODUCT_ID int mandatory
 * <li> AMOUNT int mandatory default 1
 * </ul>
 * @package Bit24\Promotions
 **/
class GiftCampaignTargetProductTable extends Own {
	public static function getFilePath()
	{
		return __FILE__;
	}

	public static function getTableName()
	{
		return 'bit24_promotions_gift_campaign_target_product';
	}

	public static function getMap()
	{
		return array(
			'CAMPAIGN_ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'validation' => array(
					__CLASS__,
					'validateCampaign'
				),
				'title' => Loc::getMessage('GIFT_CAMPAIGN_TARGET_PRODUCT_ENTITY_CAMPAIGN_FIELD'),
			),
			'PRODUCT_ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'validation' => array(
					__CLASS__,
					'validateProduct'
				),
				'title' => Loc::getMessage('GIFT_CAMPAIGN_TARGET_PRODUCT_ENTITY_PRODUCT_FIELD'),
			),
			'AMOUNT' => array(
				'data_type' => 'integer',
				'validation' => array(
					__CLASS__,
					'validateAmount'
				),
				'title' => Loc::getMessage('GIFT_CAMPAIGN_TARGET_PRODUCT_ENTITY_AMOUNT_FIELD'),
			),
			//отношения
			new Entity\ReferenceField('CAMPAIGN', 'Bit24\Promotions\GiftCampaign', array('=this.CAMPAIGN_ID' => 'ref.ID')),
			new Entity\ReferenceField('PRODUCT', 'Bitrix\Sale\Product', array('=this.PRODUCT_ID' => 'ref.ID')),
		);
	}

	/**
	 * Валидатор количества
	 * @return Entity\IValidator[]
	 */
	public static function validateAmount()
	{
		return array(
			new Entity\Validator\Length(1, null, array("MIN" => "Не указано количество")),
			new Entity\Validator\Range(1, null, false, array("MIN" => "Количество должно быть больше нуля")),
		);
	}

	/**
	 * Валидатор товара
	 * @return Entity\IValidator[]
	 */
	public static function validateProduct()
	{
		return array(
			new Entity\Validator\Length(1, null, array("MIN" => "Не указан товар")),
			new Entity\Validator\Range(1, null, false, array("MIN" => "Некорректный товар")),
		);
	}

	/**
	 * Валидатор кампании
	 * @return Entity\IValidator[]
	 */
	public static function validateCampaign()
	{
		return array(
			new Entity\Validator\Length(1, null, array("MIN" => "Не указана кампания")),
			new Entity\Validator\Range(1, null, false, array("MIN" => "Не указана кампания")),
		);
	}

	/**
	 * Возвращает все цели для кампании  $campaignId
	 *
	 * @param int  $campaignId      ID кампании
	 * @param bool $withExtraFields возвращать название и ID инфоблока товара
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getForCampaign($campaignId, $withExtraFields = false)
	{
		$select = array("*");
		if ($withExtraFields)
		{
			$select = array_merge($select, array(
				"NAME" => "PRODUCT.IBLOCK.NAME",
				"IBLOCK_ID" => "PRODUCT.IBLOCK.IBLOCK_ID"
			));
		}

		return self::getList(array(
			"select" => $select,
			"filter" => array("=CAMPAIGN_ID" => $campaignId)
		))->fetchAll();
	}

	/**
	 * Удаляет все цели кампании $campaignId
	 *
	 * @param int $campaignId ID кампании
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function deleteForCampaign($campaignId)
	{
		$cursor = self::getList(array(
			"select" => array(
				"CAMPAIGN_ID",
				"PRODUCT_ID"
			),
			"filter" => array("=CAMPAIGN_ID" => $campaignId)
		));

		while ($row = $cursor->fetch())
		{
			static::delete($row);
		}
	}
}
