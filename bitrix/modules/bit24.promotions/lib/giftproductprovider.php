<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Pavel Mashanov
 */

namespace Bit24\Promotions;

/**
 * Набор обработчиков торгового каталога для товара-подарка
 *
 * Class GiftProductProvider
 * @package Bit24\Promotions
 */
class GiftProductProvider extends \CCatalogProductProvider {

	/**
	 * Обработчик на получение информации о товаре.
	 *
	 * @param array $arParams
	 *
	 * @return array
	 */
	public static function GetProductData($arParams)
	{
		//Подарок всегда можно купить
		$r["CAN_BUY"] = "Y";

		return $r;
	}

	/**
	 * Обработчик на заказ товара
	 *
	 * @param array $arParams
	 *
	 * @return array
	 */
	public static function OrderProduct($arParams)
	{
		$r = array();
		$r["CAN_BUY"] = "Y";

		return $r;
	}
}